﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{
    class Program
    {
        static void Main(string[] args)
        {
            Tanja t = new Tanja();

            Console.Write(t.Show2()); // Variante 1

            //t.Show(); // Variante 2

            Console.ReadKey();
        }
    }
}
