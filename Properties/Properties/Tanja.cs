﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties
{
    class Tanja
    {
        string name = "Tanja";
        
        public string Name
        {
            get { return name; }
            private set
            {
                name = value;
            }
        }

        // syntax property
        // private <type> <name>;
        // <modifier> <type> <Name> { [modifier] get { return <name>; } [modifier] set { <name> = value; } }

        // syntax delegate
        // <modifier> delegate <return type> <name>(<parameter>);

        // syntax event
        // <modifier> event <delegate name> <name>;

        public void Show() // Variante 2
        {
            Name = "Peter Kurz!";
            Console.Write(Name);
        }

        public string Show2() // Variante 1
        {
            return Name = "Peter Kurz!";
        }

    }
}
